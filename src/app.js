const express = require('express');
const app = express();

if(process.env.NODE_ENV === 'development') console.log('Environment:', process.env);

// PRO TIP: validate the environment and simply exit with a non-zero code
// if it isn't complete. This could be expanded to trying to connect to
// external dbs, apis, etc.
//
// This aids in deploying via orchestration. If you exit with errors
// it will bomb out a bad deployment as the orchestrator (swarm/kub) will
// not flush the old services until the new services start and run.
// Immediately exiting as soon as it's known there's a problem makes things
// work faster.
// 
// The suggestion at Docker Con was to just throw an unhandled error
if(!process.env.REDIS_HOST) throw new Error('REDIS_HOST not set');

// NOT A DOCKER PRO TIP but I like this pattern: name all the functions even if
// they are defined inline because stack traces get easier to read:
// 
//      Error: uh oh
//          at respondToAnything (/usr/src/app.js:n:n)
//             ^^^^^^^^^^^^^^^^^
//                              \
//                               omg
app.use(respondToAnything = (req,res,next)=>{
    // throw new Error('uh oh');
    res.json({werd: 'asdf'});
});

app.listen(3000,listenCallback = ()=>{
    console.log('Listening: 3000');
});

process.on('uncaughtException', (e) => {
    console.error(`${(new Date()).toUTCString()} uncaughtException:`, e.message);
    console.error(e.stack);
    process.exit(1);
});