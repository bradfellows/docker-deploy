FROM node:8.11-alpine as node

# builder stage is responsible for setting up the dependencies
FROM node as builder
# PRO TIP: set your required envvars to a blank string
# so you can set and validate them at runtime
ENV REDIS_HOST=
ENV NODE_ENV=development
WORKDIR /usr/src
# PRO TIP: copy the package.json and do an npm i first before copying code
# to maximize Docker's build cache - the idea is that the dependencies will
# change less than the src so put this higher in the file
COPY ./package.json .
RUN npm install
# then copy the src since it takes a lot less time to do this than npm i
COPY . .
RUN which node
EXPOSE 3000

# development stage is responsible for taking the built app and running it as a dev environment
FROM builder as development
CMD [ "/usr/src/node_modules/.bin/nodemon", "/usr/src/src/app.js" ]

# responsible for running the test suite
# if building test stage or further without cache, this will happen automatically
# if you run an image build to this target, it will act exactly the same as dev, but it will
# run the tests, so it's kind of useless to run.
FROM builder as test
RUN npm run test
CMD [ "echo", "\"DANGER WILL ROBINSON!!!! Don't run against this target; it is meant to run unit tests and that's it; use development target instead.\""]

# production stage is responsible for running the app in a deployed environment
# we do not base it off the builder stage because we do not necessarily want all of the
# dev tooling
FROM node:8.11-alpine as production
ENV NODE_ENV=production
WORKDIR /usr/src
COPY --from=builder /usr/src/package.json .
RUN npm install --only=production
COPY --from=builder /usr/src/src/ ./src
CMD [ "node", "src/app.js" ]

# integration stage is responsible for adding any fixtures we might need to run API tests against
# FROM production as integration
